# 群租房空调单独计费方案

#### 前言
对于群租房，如果每个房间有单独的电表计费，这个方案不适合你。

#### 目的
1.  节约用电
2.  公平（开空调比不开空调的多交钱，开空调时间长的比时间短的多交钱，空调温度低的比温度高的多交钱）。

#### 现状
1. 盖着被子开空调，大家一起买单，自己赚。
2. 看见别人开空调，自己不开，自己亏。
3. 想象着别人可能开空调，自己不开，自己亏。

#### 后果
1.  不需要开空调也开，屋里无人也开，温度能调多低就调多低！这对积极节约用电者（能不开空调就不开，能开风扇就不开空调）是极大的伤害。最终的结果就是大家都会开空调，谁都不可能占到便宜，每个月的电费会浪费很多，每个人都会有损失。
2.  更有甚者，如果出门之前忘记关空调，长时间无人房间空调浪费的电让大家一同承担是不科学的。

#### 材料  
小米温度计2一只（小米商城上有，具体功能自行搜索）

#### 方案
根据用空调时长计费，计费分两档
1.  第一档：开空调之后，如果温度计显示大于等于28度，计费：0.8元/小时（具体算法见相关解答）
2.  第二档：开空调之后，如果温度计显示小于28度，计费：1.2元/小时（具体算法见相关解答）

#### 相关解答
1.  空调计费算法：  
第一档：  
1.5匹空调功率：1.5*0.735=1.1025（度/小时）  
1.5匹空调每小时耗电费用：0.7883（元/度）*1.1025=0.8691（元/小时），取值：0.8（往小取值）  
（普通的风扇功率是60W，1.5匹的空调1个小时的耗电量差不多风扇可以使用20小时。）  
第二档：  
2.0匹空调功率：2*0.735=1.47（度/小时）  
2.0匹空调每小时耗电费用：0.7883（元/度）*1.47=1.1588（元/小时），取值：1.2（往大取值）  

2.  空调计费算法为何要分两档  
提倡节约用电。  
空调设定温度越低越费电。  
个人要求不一样，区分“不热”与“凉爽”。  

3.  为何是28度  
28度就是“不热”；如果你想要“凉爽”，请调到28度以下。

4.  空调计费算法是否可以调整  
可以。试行一段时间，根据实际情况可以随时做出调整，在大家同意之后实施。

5.  有人不同意空调另外计费怎么办  
说出具体原因，根据实际情况讨论。  
坚决抵制：浪费电者，一心想薅大伙羊毛者！  
原则：少数服从多数。  

6.  如何安装、如何计算开空调的时长  
将温度计粘贴在门（面向卧室内）上，通过米家app在门外就能随时获取每日温度曲线，将使用空调的时间计算出来。  
温度计可以存储90天的数据，精确到每小时的温度。  

7.  空调费用如何结算  
初期每周结算一次（时间长了会忘记自己开空调的时间，不方便核对），主要看这种计算方法是否准确可行。如果没有问题，后面可以按月结算。

8.  计算时长是否透明  
温度计可以绑定到米家app上，具体的温度数据会由绑定者分享给大家看到。

9.  如何判断某个屋子三天前是否开空调了  
看所有屋子三天前的温度曲线，可以得到不开空调的温度（正常气温），再与这个屋子温度曲线对比，就能得到是否开空调，以及开空调的时长。

10.  如果正常气温就低于28度怎么办  
只要你的温度计读数与其它的温度计显示相同，就不会认为是开空调，就不会计费。

11.  谁来计算空调费用  
谁都可以。计算出的结果会公布到群里面，方便大家核对，有异议要立即提出。

12.  温度以空调遥控器温度计算可以吗  
不行，必须以小米温度计显示为准。一定要注意：空调遥控器调到28度，温度计显示不一定是28度！！

13.  大约能节约多少钱  
自己根据实际情况估算。

14.  如果有人作弊怎么办  
1、将温度计固定+拍照，拆卸之后无法还原  
2、如果移走温度计，app就发现不了温度计  
3、观看温度曲线，可以发现异常  
4、相互监督，维护大家共同利益  
5、增加作弊成本（惩罚），发现第一次，计算以前所有异常数据费用的2倍；第二次：4倍；第三次：8倍。  
